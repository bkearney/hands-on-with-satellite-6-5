# Hands On with Satellite 6.5 Lab guide

## Table of Contents

- [Intro to Satellite](#intro-to-satellite)
- [Lab Environment Details](#lab-environment-details)
- [Accessing your Lab Environment](#accessing-your-lab-environment)
- [Red Hat Enterprise Linux 8 Provisioning](#red-hat-enterprise-linux-8-provisioning)
- [Manage a RHEL8 Machine with System Roles and Insights](#manage-a-rhel8-machine-with-system-roles-and-insights)
- [RHEL8 Modularity With Applicable Errata Reporting](#rhel8-modularity-with-applicable-errata-reporting)
- [Import Report Templates from Community Repository](#import-report-templates-from-community-repository)
- [RHEL8 Compliance Reporting](#rhel8-compliance-reporting)
- [Deploying a Tomcat Application on RHEL8](#deploying-a-tomcat-application-on-rhel8)
- [Create Sync Plan with Custom Cron Syntax](#create-sync-plan-with-custom-cron-syntax)
- [Container Registry Credential Management](#container-registry-credential-management)
- [Registering the Provisioned RHEL8 Host and Enabling Repositories Through System Purpose](#registering-the-provisioned-rhel8-host-and-enabling-repositories-through-system-purpose)
- [Audit the Actions Taken During the Lab](#audit-the-actions-taken-during-the-lab)
- [Export and Import a Content View](#export-and-import-a-content-view)

## Intro to Satellite
Coming soon!

## Lab Environment Details
Coming Soon!

## Accessing your Lab Environment

Every attendee gets her/his own lab environment. The labs have already been deployed, to access your lab you need a unique *Identifier (GUID)* that will be part of the hostnames and URL's you need to access.

### Get *GUID* and Access Information

- The web browser on your laptop should show information about the lab.
- Choose the lab `Hands On With Satellite 6.5` and enter `spaceship` for the activation key
- After submitting your input by clicking *Next* you will see the attendee welcome screen with all the information you need:
    - The most important part: Your unique lab *GUID*, which will look something like `a1b2`
    - A *link* to the *lab guide* 
    - The *hostnames / URLs* with your *GUID* for *accessing* your lab
    - A *link* to a *status page*

### Access a host via SSH 

- The lab instructions will ask you to access the terminal on a certain machine.
- To do this, you will have to access it through the a host called the `bastion host`, which is the entrypoint to the lab environment.
- Make sure you know your *GUID*
- Open a terminal session to log in to your bastion host:
```
ssh root@host-<GUID>.rhpds.opentlc.com
```
- Use the password specified in the lab instructions
- From this host, know as the `bastion host`, you can access any of the Virtual Machines in the environment. 
- **You need to go through this host first to get to the Satellite, Capsule, and Hosts.** None of the other VMs have their ssh port exposed externally.
- The bastion host has short hostnames and ssh keys copied over for ease of use
- All of these commands can be run from the bastion host to access the machines in the lab environment:
    - To access the Satellite: `ssh satellite`
    - To access the Capsule: `ssh capsule`
    - To access the Skylab host registered to Satellite: `ssh skylab`
    - To access the Aqua host registered to Satellite: `ssh aqua`

### Access the Satellite UI
- Open a browser to access the Satellite User Interface:
```
https://satellite65-<GUID>.rhpds.opentlc.com
```
- Replace <GUID> with the GUID assigned to your seat!

### Example

- For example, If your GUID is *83d4*, run this to get to your bastion host:
```
ssh root@host-83d4.rhpds.opentlc.com
```
- Use the password specified in the lab instructions
- From there, you can get to the satellite command line:
```
ssh satellite
```
- As well as accessing other machine's command lines, like the `skylab` host:
```
ssh skylab
```
- And you can open up the Satellite UI in your web browser
```
https://satellite65-83d4.rhpds.opentlc.com
```

## Red Hat Enterprise Linux 8 Provisioning

### Provision a Host on an External Compute Resource

* Navigate to `Hosts > Create Host` to view the host creation dialog.
* On the `Host` tab select `RHEL8` from the `Host Group` drop-down list to inherit parameters from the host group. You can navigate other tabs to view the populated parameters.
* Set location to `Default Location` manually in the form
* Click `Submit`
* Observe the host build process and the host has entered the *Pending installation* build state. We will give the host a chance to complete provisioning and revisit it later in the lab.

## Manage a RHEL8 Machine with System Roles and Insights

### Install Insights on Host
* Execute the job template to install and register the insight client to the RHEL8 host `skylab` 
* Go to Insights UI and see the registered hosts
* In Overview under Actions, there will be one host with High action
* This rule should be visible: `System will fail to boot when SELINUXTYPE is set improperly or the corresponding policy package is missing`

### Fix the SElinux issue
There are two main ways (we'll settle on one for the actual lab, just documenting both since there is a workaround right now):
### via insights Planner
* Click on `Create a Plan`
* Pick the associated rule: `System will fail to boot when SELINUXTYPE is set improperly or the corresponding policy package is missing`
* Choose Specific System, and pick the affected rhel 8 host
* Click on `Run Playbook`
*KNOWN ISSUE:*
When clicking on `Run Playbook`, an alert is shown with `Failed to create job. Ensure your systems are registered in Foreman` error.
This should be resloved on the lateset snap (not yet verified)

### via selinux-rhel-system-role
* Go to all hosts
* click on the affected host
* click on `schedule remote job`
* choose `Ansible Playbook` under Job Category
* pick `selinux-system-role` playbook
* click on submit

Go to Insights -> overview, no action will be shown

## RHEL8 Modularity With Applicable Errata Reporting
*RHEL8 introduces Module Streams (often referred to as the "Application Stream" or AppStream for short). Satellite 6.5 provides a way to view, enable, and install these modules. Also, Satellite can apply Module Stream Errata to Hosts*

### View a Module Stream
*Information about synced Module Streams is shown on the new `Module Streams` page*
- Navigate to `Content > Module Streams` page
- Here you can view the Module Streams found in the Repositories synced for the Organization. 
- Search for the nodejs module using `name =  nodejs`
- Notice there is a Module Stream for versions 8 and 10, these streams can be used to only make a specific major version of nodejs available on a system 
- Click on a row to view the details page, here you will be able to see the details for a module stream
    - Details - general information about the Module Stream
    - Repositories - repositories containing the Module Stream
    - Profiles - The profiles available to this Module Stream. Enabling these on a system allow different groups of packages to be installed.
    - Artifacts - A list of all the RPMs that are included in the module stream.

### Install a module stream through Satellite
*Satellite provides the ability to install Module Streams to a specific host*
- Navigate to `Hosts > Content Hosts` and then click on `aqua.example.com` to get to the content host details page
- Go to the `Module Streams` tab and search `name = nodejs`
- For `nodejs` stream `10`, click the `Actions` dropdown and click on `Install` to install the package.
- This will launch a remote execution action. Once it completes, you can see the output from the host by clicking on `aqua.example.com` in the hosts list at the bottom of the page 
- SSH into the `aqua` host terminal and run `dnf upload-profile` (might not need to)
- Navigate to `Hosts > Content Hosts` and go back to the `aqua` content host details. 
- Click on the `Module Streams` tab and select `Installed` in the 'Filter by Status' dropdown. Notice the nodejs module stream is Enabled and Installed.

### Apply Module Stream Errata
*Satellite provides the ability to apply Module Stream Errata on a specific host. You also can view a report of the hosts that have errata available to apply. We'll use this report to view the applicable errata for host aqua*
- Navigate to `Monitor > Report Templates`
- Click 'Generate' on the `Applicable Errata` line
- In the 'Hosts filter' box, input `name = aqua.example.com`
- Click `submit` and save the file.
- Open up the file to see a CSV errata report for the `aqua` host, notice there is an applicable errata
- Navigate to `Hosts > Content Hosts` and then click on `aqua.example.com` to get to the content host details page
- In the `Packages > Installed` tab, search for `name =  go-toolset` to find the version of the package installed on the `aqua` host
    - This should be `go-toolset-1.10.3-10.module+el8+2175+e66141eb.x86_64`
- Click on the `Errata` tab
- Notice there is an installable errata for the host
- Check the row with `RHSA-2018:2015` and click the **dropdown** next to `Apply Selected` and click on `via Remote Execution`
- In the `aqua` host terminal and run `dnf upload-profile` (might not need to)
- Once the task completes, navigate to `Hosts > Content Hosts` and go back to the `aqua` content host details. 
- Go to `Errata` tab for host and verify there is no more installable errata.
- In the `Packages > Installed` tab, search for `name =  go-toolset` to find the version of the package installed
    - Notice the version has been updated: `go-toolset-1.10.3-12.module+el8+2195+f6fab0d5.x86_64`
- Go back to `Monitor > Report Templates`
- Click 'Generate' on the `Applicable Errata` line
- In the 'Hosts filter' box, input `name = aqua.example.com`
- Click `submit` and save the file.
- Open up the file to see a CSV errata report for the `aqua` host, notice there is no more applicable errata (the file will be blank)

### Extra Credit
- Install TBD module stream through  host collection actions page
- Install TBD module stream through host bulk action

## Import Report Templates from Community Repository
*In the previous exercise, we used Report Templates to view information about our hosts. These templates can be imported from the [Foreman community repo](https://github.com/theforeman/community-templates) or another git repository.*

### Importing a community templates
*Hammer can be used to import the templates and filter the templates to the just ones you want to import*
- Go to `Monitor > Report Templates` and notice there are no community templates yet
- In Satellite's terminal, run
```
hammer -u admin -p changeme import-templates --prefix 'Community - '  \
                                             --filter '.*statuses$' \
                                             --branch ea0a9cdac724a49aa6d0b89108927c6cd2c7cc50  \
                                             --organization 'Default Organization'
```
- When you see `Import finished.`, refresh the `Report Templates` page and see the new community template imported, called `Community - Host statuses`


### Additional Info
- You can also import all the community templates by remove the `--filter` flag.
- By default, [The Foreman community repo](https://github.com/theforeman/community-templates) is used to import templates, but you can import them from any git repository with `--repo`

## RHEL8 Compliance Reporting

*Satellite can be used to manage system security and standards compliance. The skylab host is set up to use a RHEL8 openSCAP compliance profile that is tailored to a limited policy for demonstration purposes*

### Fix failed hosts from report
*After a report has been generated for a host, we can fix those issues and generate a new report to ensure our system is compliant*
- In `Hosts > All Hosts`, select `skylab.example.com` host.
- Click the dropdown caret next to `Schedule Remote Job` and click on `Run OpenSCAP scan`
![testing_gif](images/openscap_scan.gif)
- Let the report finish (it will take a minute)
- Once the task has completed, navigate back to the host details page by clicking the `Host Detail` button on the `skylab.example.com` row at the bottom of the page
- Click on the `Compliance` button and then click `RHEL8 SCAP latest report` to view the compliance report
- Notice that one policy, `default crypto policy` has failed, see the summary for the command to fix
![testing screenshot](images/openscapreport.png)
- Navigate back to the host details page again by going to `Hosts > All Hosts` and selecting the `skylab.example.com` host.
- Click `Schedule Remote Job` in the host details
- Fill in the command box with `sudo update-crypto-policies --set DEFAULT`, which will fix our compliance issue
- Once the task has completed, navigate back to the host details page again by clicking the `Host Detail` button on the `skylab.example.com` row at the bottom of the page
- Generate a new compliance report again with `Run OpenSCAP scan`
- Following the same steps as above to view the report, all policies will be passing in the report now

## Deploying a Tomcat Application on RHEL8

*Satellite suports distributing custom content to Capsules and clients via file repositories, which allows for a wide range of client workflow. The following section shows how to use Satellite to distribute and deploy a versioned Tomcat application to clients.*

### Create Product and Repository

First create a product and a file repository to store the Java application content: 

* Navigate to `Content > Products`
* Click the `Create Product` button
* In the `Name` field give the name `Tomcat Apps`
* Click `Save`
* In the Product page that displays afterwards, click the `New Repository` button
* In the `Name` field give it the name `Hello Java App`
* In the `Type` drop-down list select `file`
* Click `Save` to save the repository

### Upload Java Application

On the Satellite exists a pre-built WAR file that you can now get uploaded to the file repository:

- If you are not already logged in from a previous exercise, ssh in to the Satellite VM as the root user
- Change to the root user's home directory:

```
cd /root
```
- Now upload the WAR file to the file repository using hammer:

```
hammer -u admin -p changeme repository upload-content --product "Tomcat Apps" \
                                                      --name "Hello Java App" \
                                                      --organization "Default Organization" \
                                                      --path ~/helloworld/hello.war
```
- Back in the Satellite UI, click on the repository `Hello Java App` and note it lists `Files 1` under `Content Counts`. 
- Click on the `1` to see that our `hello.war` file exists in that repository.

### Create and Publish a Composite Content View

Create a content view to make the application content accessible to Satellite clients: 

* Navigate to `Content > Content Views`
* Click the `Create New View` button
* Give it the name `Hello World Application` and click `Save`
* Click the `File Repositories` tab and then click the `Add` sub-tab
* Select the `Hello Java App` from the table and click the `Add Repositories` button
* Navigate back to the `Content Views` list for example by clicking the breadcrumb located under the `Hello World Application` title

Note in the Content Views List there is an existing content view called `RHEL8 with errata` that is used to distribute the OS content to clients. On hosts where we will be deploying our Java application, we want to have also the OS content available. One way to ensure this is by creating a *composite content view* so that the application content is distributed together with OS content:

* On the `Content Views` page click the `Create New View` button
* Give it the name `Hello World Application with RHEL8`
* Select the `Composite View?` and `Auto Publish` checkboxes and click `Save`
* Select the checkbox to the left of `RHEL8 with errata` and verify the version is set to `Always Use Latest`
* Select the checkbox to the left of `Hello World Application` and verify the version is set to `Always Use Latest`
* Click `Add Content Views`

We have now created a composite content view that is set to auto publish itself whenever a component view is updated. Thus if the RHEL8 view is re-published, or our application view receives an update, this composite will publish and stage a new view.

* Using the arrows icon next to the breadcrums `Hello World Application with RHEL8 >> Content Views`, navigate back to the `Hello World Application`. Now we will publish our application.

![breadcrumb](images/breadcrumb.png)

* Click `Publish New Version`
* Click `Save`
* Navigate back to `Hello World Application with RHEL8`

Note that `Version 1.0` is either published or still publishing via the `Status` column. If still publishing, wait until complete. Once complete, the `Description` column will show that this was due to an Auto Publish and which content view publish triggered the event.

#### Deploy Java Application

To deploy the Java application we will use an Ansible playbook executed on host via remote execution. The playbook will start the Tomcat server and place the application content we published to the appropriate directory on client:

* Navigate to `Hosts > All Hosts`
* Click on `aqua.example.com`
* Click `Schedule Remote Job`
* In the `Job Category` dropdown, select `Ansible Playbook` 
* Click `Submit`. This will take you to the Job Invocations view
* Click `aqua.example.com` in the hosts table
* This page shows the Ansible role in action running on our host. Once this is complete we can view our application.
* Wait for a minute or two and open a browser tab to `http://aqua-<GUID>.rhpds.opentlc.com:8080/hello` [Replace GUID in the URL with the GUID of your environment]
* Note a `Hello World!` application is running

#### Bonus: Deploy New Version of Java App

On the Satellite exists a newer version of our application in a pre-built WAR file that we will now upload to our repository.

- If you are not already logged in from a previous exercise, ssh in to the Satellite VM as the root user
- Change to the root user's home directory:

```
cd /root
```

- Upload the new version of the WAR file to the file repository:

```
hammer repository upload-content --product "Tomcat Apps" \
                                 --name "Hello Java App" \
                                 --organization "Default Organization" \
                                 --path  ~/hellosummit/hello.war
```

Now let's publish our application content view to produce a new version with our application:

* Navigate to `Content > Content Views`
* Click on `Hello World Application`
* Click `Publish New Version`
* Click `Save`
* Navigate back to `Hello World Application with RHEL8`

Again, watch as `Version 2.0` is either published or still publishing via the `Status` column. If still publishing, wait until complete. Once complete, the `Description` column will show that this was due to an Auto Publish and which content view publish triggered the event. In this case, it should indicate Version 2.0 of `Hello World Application`.

Now, let's re-apply our Ansible role to fetch the new version of our application:

* Navigate to `Hosts > All Hosts`
* Click on `aqua.example.com`
* Click `Schedule Remote Job`
* From the `Job Category` dropdown select `Ansible Playbook`
* Click `Submit`
* Click `aqua.example.com` in the Hosts table

This page shows the Ansible role in action running on our host. Once this is complete we can view our application.

* Open a browser tab to `http://aqua-<GUID>.rhpds.opentlc.com:8080/hello` where GUID is to be replaced with the id of your environment
* Note that the application now reads `Hello Red Hat Summit!`

## Create Sync Plan with Custom Cron Syntax
*Satellite 6.5 allows for custom cron-like syntax when creating Sync Plans for Products*

* Navigate to `Content > Sync Plans > Create Sync Plan`
* Name the sync plan `Tiny Product Sync`
* Set Interval to `custom cron` and specify a every 15-minute cron line: `*/15 * * * *`
* Click `save` (you can keep the rest of the fields the default values)
* Notice the next sync time the newly created plan displays, that is when you can check back for a successful sync
* Click on the `Products` tab for the Sync Plan
* Assign the Sync Plan to the `Tiny Product` product by checking the box next to its name and clicking `Add Selected`
* Check the recurring logic has been created under `Monitor > Recurring Logics`
* Later in the lab, verify the syncplan started the synchronization on assigned product either in `Content > Product > Tiny Product > Tasks` or in `Monitor > Tasks`

## Container Registry Credential Management
*Satellite 6.5 allows the ability to discover container images through a registry. Additionally, you can require authentication in a lifecycle environment, meaning images in that environment will need the proper authentication to be pulled by a host.* 

### Discover a Container Image from a Private Registry
*A private Quay container registry is running on `quay.example.com`. Satellite can discover images there and add them as a Repository*
* Navigate to `Content > Products`
* Click on the `Repo Discovery` button
* Under `Repository Type`, choose `Container Images`
* Under `Registry to Discover`, select `Custom` and use the URL `quay.example.com`
* Enter `summituser` for `Registry Username`
* Enter `summitpassword` for `Registry Password`
* Enter `calipso` for `Registry Search Parameter`
* Click `Discover`
* Check the box next to `summituser/calipso`
* Click `Create Repository`
* Click `Run Repository Creation`
* Navigate to newly created Repository by clicking the `Repository Created` link
* Sync repo by clicking the dropdown next to `Select Action` and clicking `Sync Now`

### Set Authentication on Lifecycle Environment
*Satellite can be configured to require authentication when using images that are in a Lifecycle Environment*
- Navigate to `Content > Lifecycle Environments`
- Select `Production` to get to it's detail page
- Ensure `Unauthenticated Pull` is `no` (this means we need login credentials to use images in this environment)
* Navigate to `Content > Content Views`

### Create Content View with Image and Promote to Production
*We will create a content view containing our discovered image and promote it to the Production Lifecycle Environment*
* Click `Create New View`
* Create a new Content View with the name `RHEL8 container tags`
* In the Content View details page, click on `Container Images > Repositories`
* In the `Add` tab, add `summituser/calipso` by selecting it and clicking `Add Repositories`
* click `Publish new version` on the top right to publish the content view
* Click on `Promote` next to `Version 1.0`
* Promote the Content View Version straight to `Production` Lifecycle Environment by checking the `Production` checkbox and clicking `Promote Version`
* Navigate to `Hosts > Content Hosts` and then click on `skylab.example.com` to get to the content host details page
* Under `Content Host Content` Change the associated content view to `RHEL8 container tags` and save

### Login to Satellite's Registry on Skylab
- In the terminal for the `skylab` host, run `docker search satellite65.example.com/calipso`
    - Notice nothing shows, this is expected because we are not logged in.
- `docker login satellite65.example.com` and login with the username `admin` and password `changeme`
- `docker search satellite65.example.com/calipso`
    -  Notice the docker tags from satellite are now showing

### Update Naming Pattern for registry
*Satellite can set a naming pattern scheme for images in a particular Lifecycle environment*
- Back in the Satellite UI, navigate to `Content > Lifecycle Environments`
- Click on `Production` to get to the details page
- Update `Registry Name Pattern` to be `<%= repository.docker_upstream_name %>` and save.

### Search and Pull Image with Shortened Name
- Back in the terminal for the `skylab` host, run `docker search satellite65.example.com/calipso` again
    - Notice the one of the images has changed its name to `summituser/calipso`. This is the image in the production content view
- Run `docker pull satellite65.example.com/summituser/calipso`
- Notice the image is now in `docker images`

### Extra Credit

- Can you customize the production image name to show up as `production/summituser/calipso`?

## Registering the Provisioned RHEL8 Host and Enabling Repositories Through System Purpose

### SSH into the provisioned host
- Go to `Hosts > All Hosts` and select the host that you created at the beginning of this lab
- If the host is powered off, select the green `Power On`
- Connect to the provisioned host via ssh, you'll need to access the host via it's IP address, found in the host details page. The host must be accessed through the capsule.
- For example, if the IP address for your provisioned host is `192.168.122.183`:
    - ssh to your bastion host
    - `ssh capsule` (on bastion host)
    - `ssh root@192.168.122.183` (on capsule)
    - log in with the root password `changeme`

### Register the host to Satellite
- Register the host as a content host. Run the following commands (be sure to replace your GUID in the satellite hostname):
```
cd ~
curl --insecure --output katello-ca-consumer-latest.noarch.rpm https://satellite65-<GUID>.rhpds.opentlc.com/pub/katello-ca-consumer-latest.noarch.rpm
```
- Install the bootstrap rpm
```
yum install -y katello-ca-consumer-latest.noarch.rpm
subscription-manager register --org="Default_Organization" --environment="Library"
```
- Enter `admin` for the username and `changeme` for the password
- After successful host registration run `subscription-manager repos` to observe that system has no repositories available through subscriptions.

### Set the system purpose on our provisioned host
- Still in the provisioned host's terminal, specify system purpose parameters

```
syspurpose set-usage Production
syspurpose set-role "Red Hat Enterprise Linux"
```

- Navigate to `Hosts > Content Hosts` and select the provisioned host. 
- From the `Subscriptions` tab dropdown select `Subscriptions`. 
- Click `Run Auto-attach`. 
- Refresh the page and observe that a subscription has been attached to the host based on the specified system purpose parameters.
- Back in the provisioned host's terminal, run `subscription-manager repos` again to see the list of repositories enabled through the attached subscription.
- You can now install packages from the enabled repos on the host. Try to install a package to test this out.

```
yum -y install vim
```


## Audit the Actions Taken During the Lab 

*Satellite 6.5 introduces the Audits Page, which allows for viewing and searching actions performed by the Satellite*

### Clone and Modify a Provisioning Template
*Modifying a template will show up with the template diff in it's audit entry*
- Navigate to `Hosts > Provisioning Templates`
- Search `name = epel`
- Click `clone` under Actions for the `epel` template
- Save template by clicking `Submit`
- This creates an `epel clone` template, click on the new `epel clone` template back in the Provisioning Templates page
- In the text editor provided on the page, edit the template to change the fallback epel mirror in the template from `https://dl.fedoraproject.org/pub/epel` to `http://mirrors.mit.edu/epel/`
- The 8th line should read:
```
repo_base  = host_param('epel-repo-base') ? host_param('epel-repo-base') : 'http://mirrors.mit.edu/epel/'
```
- Add a comment in the box below `Updated EPEL mirror`
- Save template by clicking `Submit`

### View audited action in audits UI
- Navigate to the Audits page `Monitor > Audits`
- Notice that you can audit the template changes and see the change diff
- Feel free to explore the audits UI more to see audits of the changes we made in the lab

## Export and Import a Content View
*Content views can be exported and imported from one organization to another, or from one Satellite to another. The Repositories contained in the Content View have already been synced to Another Org, and the Content View has been created. We will import the version from Default Organization's Content View*

### View the Empty Content View in `Another Org`
- Select `Another Org` from the Organization dropdown at the top of Satellite.
- Navigate `Content > Content Views > zoo`, notice no versions exist
- In Satellite's terminal, list all content view versions for the zoo Content View in the Default Organization
```
hammer -u admin -p changeme content-view version list --organization-id 1 --content-view zoo
```
- Let's export version 5.0 to `Another Org`
- Export the Content View Version using it's id 19
```
hammer -u admin -p changeme content-view version export --export-dir /var/lib/pulp/katello-export/ --id 19 
```
- This creates a tar file in `/var/lib/pulp/katello-export/` we can now import
- Get id for `Another Org` with
```
hammer -u admin -p changeme organization list
```
- Import that version to `Another Org`
```
hammer -u admin -p changeme content-view version import --export-tar /var/lib/pulp/katello-export/export-zoo-19.tar \
                                                        --organization-id 3
```
- In the UI, refresh the versions for the `zoo` content view in `Another Org` and notice the version has been imported!

### Extra Credit
- Can you import more versions from Default Organization's `zoo` content view into Another Org's `zoo` content view? 
